import { fetchJson } from "./fonction/api.js"
import { ListPokemons } from "./component/ListePokemons.js"

const pokemons = await fetchJson('https://pokeapi-enoki.netlify.app/pokeapi.json')
const carteAlea = document.querySelector(".carteAlea")
const list = new ListPokemons(pokemons)
list.appendTo(carteAlea)



