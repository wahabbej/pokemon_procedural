export async function fetchJson (url){
    const r = await fetch(url)
    if(r.ok){
        return r.json()
    }
}