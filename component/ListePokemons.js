export class ListPokemons{
    #pokemons = []
    #pokemonsAffich = []
    #pokemons2 =[]

    constructor(pokemons){
        this.#pokemons = pokemons
    }

    appendTo(element1){
        const lanceur = document.querySelector(".lanceur")
        const timer = document.querySelector(".timer")
        const stop = document.querySelector(".stop")
        const choisis = document.querySelectorAll(".choisi")
        let interval1
        let interval
        lanceur.addEventListener("click", (e)=>{
            if (this.#pokemonsAffich.length>=6){
                this.#pokemonsAffich = []
                for (let choix of choisis){
                    choix.innerHTML = this.getRow()
                    
                }
                stop.classList.add("disabled")
                lanceur.classList.remove("disabled")
            }
            interval1 = setInterval(()=>{
            e.preventDefault()
            let nb = 0
            for (let pokemon of this.#pokemons.pokemons){
                nb+=1
                this.#pokemons2.push(pokemon)
            }
            const numeroCarte = Math.floor(Math.random()*nb)
            const pokemonAlea =  this.#pokemons2[numeroCarte]
            element1.innerHTML = 
                `<div class=" col-md-8 card  texte1" data-pokemon="${pokemonAlea.id}" style="width: 19.5rem;background-color:${pokemonAlea.background_color} ;">
                    <div class=" d-flex justify-content-between mx-4 mt-3"><h5 >${pokemonAlea.name}</h5><h5>${pokemonAlea.level} ${this.getAbilities("icon",pokemonAlea)}</h5></div>
                    <div  class="mx-3  mt-5 d-flex justify-content-center  text-center card-body rempli">
                        <img  width="100%" src="${pokemonAlea.image}" alt="">
                    </div>
                    <div class="d-flex justify-content-between mt-3 mx-3">
                        <p >${this.getAbilities("icon",pokemonAlea)}</p>
                        <p class="fw-bold">${this.getAbilities("name",pokemonAlea)}</p>
                        <p class="fw-bold">${this.getAbilities("power",pokemonAlea)}</p>
                    </div>
                    <p class="text-center">${this.getAbilities("description",pokemonAlea)}</p>
                </div>
            `
            },300)
            let delai = 15
            timer.innerHTML = delai + " sec"
            interval = setInterval(()=>{
            delai=delai-1
            timer.innerHTML = delai+ " sec"
            if (delai==0){
                clearInterval(interval)
                delai = 15
                timer.innerHTML = delai + " sec"
                clearInterval(interval1)
                let dataPokemon = element1.firstChild.getAttribute("data-pokemon")
                if(!this.#pokemonsAffich.includes(dataPokemon)){
                    this.#pokemonsAffich.push(dataPokemon)
               }
                for (let pokemon of this.#pokemons.pokemons){
                    for (let i=0 ;i<this.#pokemonsAffich.length;i++){
                        if (this.#pokemonsAffich[i] == pokemon.id){
                            let carte = document.querySelector(`.choisis-${i+1}`)
                            carte.innerHTML = 
                            `<div col-md-8 card  texte " style="width: 10rem;height: 16rem;background-color:${pokemon.background_color}">
                                <div class=" d-flex justify-content-between mx-4 mt-3"><p class="texte" >${pokemon.name}</p><span class="texte">${pokemon.level}<span></span>${this.getAbilities("icon",pokemon)}  </span></div>
                                <div  class="mx-3   text-center card-body rempli ">
                                    <img class="mt-5 " width="50%"  src="${pokemon.image}" alt="">
                                </div>
                                <div>
                                    <div class="d-flex mt-3 justify-content-between mx-3 texte">
                                        <p >${this.getAbilities("icon",pokemon)}</p>
                                        <p class="fw-bold">${this.getAbilities("name",pokemon)}</p>
                                        <p class="fw-bold"> ${this.getAbilities("power",pokemon)}</p>
                                    </div>
                                    <p class="text-center  texte ">${this.getAbilities("description",pokemon)} </p>
                                </div>
                            </div>`
                        }   
                    }
                stop.classList.add("disabled")
                lanceur.classList.remove("disabled")
                }
            }
         },1000)
        stop.classList.remove("disabled")
        e.currentTarget.classList.add("disabled")
    })        
    stop.addEventListener("click", (e)=>{
        e.preventDefault()
        clearInterval(interval)
        clearInterval(interval1)
        let dataPokemon = element1.firstChild.getAttribute("data-pokemon")
        if(!this.#pokemonsAffich.includes(dataPokemon)){
             this.#pokemonsAffich.push(dataPokemon)
        }
        for (let pokemon of this.#pokemons.pokemons){
            for (let i=0 ;i<this.#pokemonsAffich.length;i++){
                if (this.#pokemonsAffich[i] == pokemon.id){
                    let carte = document.querySelector(`.choisis-${i+1}`)
                    carte.innerHTML = 
                    `<div col-md-8 card  texte " style="width: 10rem;height: 16rem;background-color:${pokemon.background_color}">
                    <div class=" d-flex justify-content-between mx-4 mt-3"><p class="texte" >${pokemon.name}</p><span class="texte">${pokemon.level}<span></span>${this.getAbilities("icon",pokemon)}  </span></div>
                    <div  class="mx-3   text-center card-body rempli ">
                        <img class="mt-5 " width="50%"  src="${pokemon.image}" alt="">
                    </div>
                    <div>
                        <div class="d-flex mt-3 justify-content-between mx-3 texte">
                            <p >${this.getAbilities("icon",pokemon)}</p>
                            <p class="fw-bold">${this.getAbilities("name",pokemon)}</p>
                            <p class="fw-bold"> ${this.getAbilities("power",pokemon)}</p>
                        </div>
                        <p class="text-center  texte ">${this.getAbilities("description",pokemon)} </p>
                    </div>
                </div>`
            }   
            }
        e.currentTarget.classList.add("disabled")
        lanceur.classList.remove("disabled")
        }  
    })    
    for (let choix of choisis){
        choix.innerHTML = this.getRow()
    }   
    }



    getAbilities(name,pokemon){
        for (let abilitie of pokemon.abilities){
            for (const [attribut,value] of Object.entries(abilitie) ){
                if(attribut==name){
                    return value
                }
            }
        }  
    }

    getRow(){
        return `<div class=" col-md-8 card texte " style="width: 10rem;height: 16rem;">
        <div class=" d-flex justify-content-between mx-4 mt-3"><p class="fw-bold" ></p><span class="fw-bold"><span></span>  </span></div>
        <div  class="mx-3   text-center card-body ">
            <img class="mt-5" width="50%"  src="" alt="">
        </div>
        <div>
            <div class="d-flex mt-3 justify-content-between mx-3 texte">
                <p ></p>
                <p class="fw-bold"></p>
                <p class="fw-bold"></p>
            </div>
            <p class="text-center  texte ">t</p>
        </div>
    </div>`
    }

    
}